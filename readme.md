# DateTimeProvider

## Installation
The best way to install Slts/DateTimeProvider is using  [Composer](https://getcomposer.org/):

```sh
$ composer require slts/datetime-provider
```

## Configuration

### Nette
```
extensions:
    dateTimeProvider: Slts\DateTimeProvider\DI\Nette2DateTimeProviderExtension

dateTimeProvider:
    defaultType: current    # default: request
```

### Symfony
```
```
