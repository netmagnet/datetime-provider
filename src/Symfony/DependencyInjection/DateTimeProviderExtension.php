<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider\Symfony\DependencyInjection;

use Slts\DateTimeProvider\Provider\ConstantProvider;
use Slts\DateTimeProvider\Provider\CurrentProvider;
use Slts\DateTimeProvider\Provider\MutableProvider;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use function assert;

class DateTimeProviderExtension extends Extension
{
    public const SERVICE_NAME = 'slts.datetime_provider.provider';

    public function getAlias() : string
    {
        return 'slts_datetime_provider';
    }

    /**
     * @param mixed[][] $configs
     */
    public function load(array $configs, ContainerBuilder $container) : void
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $this->resolveProvider($config, $container);
    }

    /**
     * @param mixed[] $config
     */
    private function resolveProvider(array $config, ContainerBuilder $container) : void
    {
        switch ($config['type']) {
            case Configuration::TYPE_CURRENT:
                $this->registerAlias($container, CurrentProvider::class);
                return;
            case Configuration::TYPE_REQUEST_TIME:
                $this->registerAlias($container, ConstantProvider::class);

                return;
            case Configuration::TYPE_MUTABLE_REQUEST_TIME:
                $this->registerAlias($container, MutableProvider::class);
                return;
        }

        assert(false);
    }

    private function registerAlias(ContainerBuilder $container, string $alias) : void
    {
        $container->setAlias(self::SERVICE_NAME, new Alias($alias, true));
    }
}
