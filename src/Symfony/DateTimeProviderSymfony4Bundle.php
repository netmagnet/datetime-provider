<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider\Symfony;

use Slts\DateTimeProvider\Symfony\DependencyInjection\DateTimeProviderExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DateTimeProviderSymfony4Bundle extends Bundle
{
    public function getContainerExtension(): ExtensionInterface
    {
        if ($this->extension === null) {
            $this->extension = new DateTimeProviderExtension();
        }

        return $this->extension;
    }
}
