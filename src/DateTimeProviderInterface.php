<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider;

use DateTimeImmutable;

interface DateTimeProviderInterface
{
    public function getDateTime() : DateTimeImmutable;
}
