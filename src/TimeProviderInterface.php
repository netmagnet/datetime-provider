<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider;

use DateInterval;

interface TimeProviderInterface
{
    public function getTime() : DateInterval;
}
