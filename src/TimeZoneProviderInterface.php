<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider;

use DateTimeZone;

interface TimeZoneProviderInterface
{
    public function getTimeZone() : DateTimeZone;
}
