<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider;

use DateTimeImmutable;

interface DateProviderInterface
{
    public function getDate() : DateTimeImmutable;
}
