<?php

namespace Slts\DateTimeProvider\Exception;

class InvalidStateException extends \RuntimeException implements \Slts\DateTimeProvider\Exception
{
}
