<?php

namespace Slts\DateTimeProvider\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements \Slts\DateTimeProvider\Exception
{
}
