<?php

namespace Slts\DateTimeProvider\Exception;

class NotImplementedException extends \LogicException implements \Slts\DateTimeProvider\Exception
{
}
