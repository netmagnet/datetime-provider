<?php

namespace Slts\DateTimeProvider\Exception;

class UnexpectedValueException extends \UnexpectedValueException implements \Slts\DateTimeProvider\Exception
{
}
