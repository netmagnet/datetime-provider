<?php

namespace Slts\DateTimeProvider\TimeAccessor;

use DateTimeImmutable;
use DateTimeZone;
use function date_default_timezone_get;
use function sprintf;

class RequestTimeAccessor
{
    public function getRequestTime(): DateTimeImmutable
    {
        return (new DateTimeImmutable(sprintf('@%.6f', $_SERVER['REQUEST_TIME_FLOAT'])))
            ->setTimezone(new DateTimeZone(date_default_timezone_get()))
        ;
    }
}
