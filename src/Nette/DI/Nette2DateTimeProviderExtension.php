<?php

namespace Slts\DateTimeProvider\Nette\DI;

use Nette\DI\CompilerExtension;
use Slts\DateTimeProvider\DateProviderInterface;
use Slts\DateTimeProvider\DateTimeProviderInterface;
use Slts\DateTimeProvider\TimeProviderInterface;
use Slts\DateTimeProvider\TimeZoneProviderInterface;

class Nette2DateTimeProviderExtension extends CompilerExtension
{
    private const TYPE_REQUEST = 'request';
    private const TYPE_MUTABLE_REQUEST = 'mutableRequest';
    private const TYPE_CURRENT = 'current';

    private $defaults = [
        'defaultType' => self::TYPE_REQUEST,
    ];

    public function loadConfiguration()
    {
        $config = $this->validateConfig($this->defaults);
        $builder = $this->getContainerBuilder();
        $loaded = $this->loadFromFile(__DIR__ . '/../Resources/datetime.config.neon');
        $this->loadDefinitionsFromConfig($loaded['services']);

        if (null !== $config['defaultType']) {
            $defaultDefinition = $builder->getDefinition($this->prefix("{$config['defaultType']}Provider"));
            $autowired = array_merge(
                $defaultDefinition->getAutowired(),
                [
                    DateTimeProviderInterface::class,
                    DateProviderInterface::class,
                    TimeProviderInterface::class,
                    TimeZoneProviderInterface::class,
                ]
            );
            $defaultDefinition->setAutowired($autowired);
        }
    }
}
