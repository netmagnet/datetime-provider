<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider\Provider;

use DateTimeImmutable;
use Slts\DateTimeProvider\DateProviderInterface;
use Slts\DateTimeProvider\DateTimeProviderInterface;
use Slts\DateTimeProvider\TimeProviderInterface;
use Slts\DateTimeProvider\TimeZoneProviderInterface;

class CurrentProvider implements
    DateTimeProviderInterface,
    DateProviderInterface,
    TimeProviderInterface,
    TimeZoneProviderInterface
{
    use ProviderTrait;

    /**
     * {@inheritdoc}
     */
    public function getPrototype() : DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}
