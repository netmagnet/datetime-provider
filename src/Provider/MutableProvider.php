<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider\Provider;

use DateTimeImmutable;
use Slts\DateTimeProvider\DateProviderInterface;
use Slts\DateTimeProvider\DateTimeProviderInterface;
use Slts\DateTimeProvider\TimeProviderInterface;
use Slts\DateTimeProvider\TimeZoneProviderInterface;

class MutableProvider implements
    DateTimeProviderInterface,
    DateProviderInterface,
    TimeProviderInterface,
    TimeZoneProviderInterface
{
    use ProviderTrait;

    /**
     * @var \DateTimeImmutable
     */
    private $prototype;

    public function __construct(DateTimeImmutable $prototype)
    {
        $this->prototype = $prototype;
    }

    protected function getPrototype() : DateTimeImmutable
    {
        return $this->prototype;
    }

    public function changePrototype(DateTimeImmutable $prototype) : void
    {
        $this->prototype = $prototype;
    }
}
