<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider\Provider;

use DateInterval;
use DateTimeImmutable;
use DateTimeZone;
use function sprintf;

/**
 * Base implementation for DateTime-based providers.
 */
trait ProviderTrait
{
    abstract protected function getPrototype() : DateTimeImmutable;

    /**
     * {@inheritdoc}
     */
    public function getDate() : DateTimeImmutable
    {
        return $this->getPrototype()->setTime(0, 0, 0, 0);
    }

    /**
     * {@inheritdoc}
     */
    public function getTime() : DateInterval
    {
        $interval    = new DateInterval(sprintf('PT%dH%dM%dS', $this->getPrototype()->format('G'), $this->getPrototype()->format('i'), $this->getPrototype()->format('s')));
        $interval->f = $this->getPrototype()->format('u');

        return $interval;
    }

    /**
     * {@inheritdoc}
     */
    public function getDateTime() : DateTimeImmutable
    {
        return $this->getPrototype();
    }

    /**
     * {@inheritdoc}
     */
    public function getTimeZone() : DateTimeZone
    {
        return $this->getPrototype()->getTimezone();
    }
}
