<?php

declare(strict_types=1);

namespace Slts\DateTimeProvider\Provider;

use DateTimeImmutable;
use Slts\DateTimeProvider\DateProviderInterface;
use Slts\DateTimeProvider\DateTimeProviderInterface;
use Slts\DateTimeProvider\TimeProviderInterface;
use Slts\DateTimeProvider\TimeZoneProviderInterface;

class ConstantProvider implements
    DateTimeProviderInterface,
    DateProviderInterface,
    TimeProviderInterface,
    TimeZoneProviderInterface
{
    use ImmutableProviderTrait;

    /**
     * @var \DateTimeImmutable
     */
    private $prototype;

    public function __construct(DateTimeImmutable $dateTime)
    {
        $this->prototype = $dateTime;
    }

    protected function getPrototype() : DateTimeImmutable
    {
        return $this->prototype;
    }
}
