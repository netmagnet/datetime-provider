<?php

namespace TimeAccessor;

use PHPUnit\Framework\TestCase;
use Slts\DateTimeProvider\TimeAccessor\RequestTimeAccessor;

class RequestTimeAccessorTest extends TestCase
{
    public function test() : void
    {
        $_SERVER['REQUEST_TIME_FLOAT'] = 123456789.123456;

        $this->assertSame('123456789.123456', (new RequestTimeAccessor())->getRequestTime()->format('U.u'));
    }
}
