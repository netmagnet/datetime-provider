<?php

declare(strict_types = 1);

namespace Slts\DateTimeProvider\Tests\Provider;

use DateTime;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Slts\DateTimeProvider\Provider\ConstantProviderFactory;
use stdClass;

class ConstantProviderFactoryTest extends TestCase
{
    public function testCreateFromNumeric(): void
    {
        $tp = (new ConstantProviderFactory())->create(1379123601);
        $datetime = $tp->getDateTime();
        $date = $tp->getDate();
        $time = $tp->getTime();
        $timezone = $tp->getTimeZone();

        sleep(2);

        self::assertInstanceOf(DateTimeImmutable::class, $datetime);
        self::assertSame($datetime, $tp->getDateTime());
        self::assertSame($date, $tp->getDate());
        self::assertSame($time->format('%h:%i:%s.%f'), $tp->getTime()->format('%h:%i:%s.%f'));
        self::assertSame($timezone->getName(), $tp->getTimeZone()->getName());
    }

    public function testCreateFromMutableDatetime(): void
    {
        $tp = (new ConstantProviderFactory())->create(new DateTime('2013-09-14 03:53:21'));
        $datetime = $tp->getDateTime();
        $date = $tp->getDate();
        $time = $tp->getTime();
        $timezone = $tp->getTimeZone();

        sleep(2);

        self::assertInstanceOf(DateTimeImmutable::class, $datetime);
        self::assertSame($datetime, $tp->getDateTime());
        self::assertSame($date, $tp->getDate());
        self::assertSame($time->format('%h:%i:%s.%f'), $tp->getTime()->format('%h:%i:%s.%f'));
        self::assertSame($timezone->getName(), $tp->getTimeZone()->getName());
    }

    public function testCreateFromMutableDatetimeImmutable(): void
    {
        $tp = (new ConstantProviderFactory())->create(new DateTimeImmutable('2013-09-14 03:53:21'));
        $datetime = $tp->getDateTime();
        $date = $tp->getDate();
        $time = $tp->getTime();
        $timezone = $tp->getTimeZone();

        sleep(2);

        self::assertInstanceOf(DateTimeImmutable::class, $datetime);
        self::assertSame($datetime, $tp->getDateTime());
        self::assertSame($date, $tp->getDate());
        self::assertSame($time->format('%h:%i:%s.%f'), $tp->getTime()->format('%h:%i:%s.%f'));
        self::assertSame($timezone->getName(), $tp->getTimeZone()->getName());
    }

    public function testTimezones(): void
    {
        date_default_timezone_set('Europe/Prague');

        $tp = (new ConstantProviderFactory())->create(1379123601);
        self::assertSame('Europe/Prague', $tp->getTimeZone()->getName());
        self::assertSame('2013-09-14 03:53:21.000000 +02:00', $tp->getDateTime()->format('Y-m-d H:i:s.u P'));

        date_default_timezone_set('Europe/London');

        $tp = (new ConstantProviderFactory())->create(1379123601);
        self::assertSame('Europe/London', $tp->getTimeZone()->getName());
        self::assertSame('2013-09-14 02:53:21.000000 +01:00', $tp->getDateTime()->format('Y-m-d H:i:s.u P'));
    }

    public function testCreateFromUnknownException(): void
    {
        $this->expectException(\Slts\DateTimeProvider\Exception\NotImplementedException::class);
        $this->expectExceptionMessage('Cannot process datetime in given format "blablabla"');
        (new ConstantProviderFactory())->create('blablabla');
    }

    public function testCreateFromUnknownException2(): void
    {
        $this->expectException(\Slts\DateTimeProvider\Exception\NotImplementedException::class);
        $this->expectExceptionMessage('Cannot process datetime from given value stdClass');
        (new ConstantProviderFactory())->create(new stdClass());
    }

    public function testCreateFromMicroseconds(): void
    {
        $tp = (new ConstantProviderFactory())->create(1379123601.123456);
        $datetime = $tp->getDateTime();
        $date = $tp->getDate();
        $time = $tp->getTime();
        $timezone = $tp->getTimeZone();

        usleep(100);

        self::assertInstanceOf(DateTimeImmutable::class, $datetime);
        self::assertSame($datetime, $tp->getDateTime());
        self::assertSame($date, $tp->getDate());
        self::assertSame('123456', $datetime->format('u'));
        self::assertSame($time->format('%h:%i:%s.%f'), $tp->getTime()->format('%h:%i:%s.%f'));
        self::assertSame($timezone->getName(), $tp->getTimeZone()->getName());
    }
}
