<?php

declare(strict_types = 1);

namespace Slts\DateTimeProvider\Tests\Provider;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Slts\DateTimeProvider\Provider\MutableProvider;

class MutableProviderTest extends TestCase
{
	public function testConstant(): void
	{
		$tp = new MutableProvider(new DateTimeImmutable('2013-09-14 03:53:21'));
		$datetime = $tp->getDateTime();
		$date = $tp->getDate();
		$time = $tp->getTime();
		$timezone = $tp->getTimeZone();

		sleep(2);

		self::assertSame($datetime, $tp->getDateTime());
		self::assertSame($date->getTimestamp(), $tp->getDate()->getTimestamp());
		self::assertSame($time->format('%h:%i:%s.%f'), $tp->getTime()->format('%h:%i:%s.%f'));
		self::assertSame($timezone->getName(), $tp->getTimeZone()->getName());
	}

	public function testTimezones(): void
	{
		date_default_timezone_set('Europe/Prague');

		$tp = new MutableProvider(new DateTimeImmutable(date('Y-m-d H:i:s', 1379123601)));
		self::assertSame('Europe/Prague', $tp->getTimeZone()->getName());
		self::assertSame('2013-09-14 03:53:21 +02:00', $tp->getDateTime()->format('Y-m-d H:i:s P'));

		date_default_timezone_set('Europe/London');

		$tp = new MutableProvider(new DateTimeImmutable(date('Y-m-d H:i:s', 1379123601)));
		self::assertSame('Europe/London', $tp->getTimeZone()->getName());
		self::assertSame('2013-09-14 02:53:21 +01:00', $tp->getDateTime()->format('Y-m-d H:i:s P'));
	}

	public function testChangePrototype(): void
	{
		$tp = new MutableProvider($originalTime = new DateTimeImmutable('2013-09-14 03:53:21'));
		self::assertSame($originalTime, $tp->getDateTime());

		$tp->changePrototype($changedTime = new DateTimeImmutable('2015-01-09 18:34:00'));
		self::assertNotEquals($originalTime, $tp->getDateTime());
		self::assertSame($changedTime, $tp->getDateTime());
	}

	public function testMicroseconds(): void
	{
		$tp = new MutableProvider(new DateTimeImmutable('2013-09-14 03:53:21.123456'));
		$datetime = $tp->getDateTime();
		$date = $tp->getDate();
		$time = $tp->getTime();
		$timezone = $tp->getTimeZone();

		usleep(100);

		self::assertSame($datetime, $tp->getDateTime());
		self::assertSame($date->getTimestamp(), $tp->getDate()->getTimestamp());
		self::assertSame($time->format('%h:%i:%s.%f'), $tp->getTime()->format('%h:%i:%s.%f'));
		self::assertSame($timezone->getName(), $tp->getTimeZone()->getName());
	}

}
