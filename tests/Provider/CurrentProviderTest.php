<?php

declare(strict_types = 1);

namespace Slts\DateTimeProvider\Tests\Provider;

use DateTimeImmutable;
use Slts\DateTimeProvider\Provider\CurrentProvider;
use PHPUnit\Framework\TestCase;

class CurrentProviderTest extends TestCase
{

	public function testNotConstant(): void
	{
		$tp = new CurrentProvider();
		$date = $tp->getDate();
		$datetime = $tp->getDateTime();
		$time = $tp->getTime();
		$timezone = $tp->getTimeZone();

		sleep(2);

		self::assertInstanceOf(DateTimeImmutable::class, $datetime);
		self::assertSame('00:00:00.000000', $date->format('H:i:s.u'));
		self::assertNotEquals($datetime, $tp->getDateTime());
		self::assertNotEquals($time->format('%h:%i:%s.%f'), $tp->getTime()->format('%h:%i:%s.%f'));
		self::assertSame($timezone->getName(), $tp->getTimeZone()->getName());
	}

	public function testTimezones(): void
	{
		date_default_timezone_set('Europe/Prague');

		$tp = new CurrentProvider();
		self::assertSame('Europe/Prague', $tp->getTimeZone()->getName());

		date_default_timezone_set('Europe/London');

		$tp = new CurrentProvider();
		self::assertSame('Europe/London', $tp->getTimeZone()->getName());
	}

}
