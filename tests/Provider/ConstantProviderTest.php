<?php

declare(strict_types = 1);

namespace Slts\DateTimeProvider\Tests\Provider;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Slts\DateTimeProvider\Provider\ConstantProvider;

class ConstantProviderTest extends TestCase
{

	public function testConstant(): void
	{
		$tp = new ConstantProvider(new DateTimeImmutable('2013-09-14 03:53:21.123456'));
		$datetime = $tp->getDateTime();
		$date = $tp->getDate();
		$time = $tp->getTime();
		$timezone = $tp->getTimeZone();

		sleep(2);

		self::assertSame($datetime, $tp->getDateTime());
		self::assertSame($date->getTimestamp(), $tp->getDate()->getTimestamp());
		self::assertSame($time->format('%h:%i:%s.%f'), $tp->getTime()->format('%h:%i:%s.%f'));
		self::assertSame($timezone->getName(), $tp->getTimeZone()->getName());
	}

}
