<?php

require_once __DIR__ . '/../vendor/autoload.php';

// configure environment
date_default_timezone_set('Europe/Prague');

$_SERVER = array_intersect_key($_SERVER, array_flip([
    'PHP_SELF',
    'SCRIPT_NAME',
    'SERVER_ADDR',
    'SERVER_SOFTWARE',
    'HTTP_HOST',
    'DOCUMENT_ROOT',
    'OS',
    'argc',
    'argv',
]));
$_SERVER['REQUEST_TIME'] = 1234567890;

$_ENV = $_GET = $_POST = [];
